class ContactUsmailer < ActionMailer::Base
  #default from: "from@example.com"

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.contact_usmailer.contactus.subject
  #
  def contactus(qucikcontact)
    @qucikcontact= qucikcontact
     mail(:to => @qucikcontact.email, :subject => "Quick Contact Message", :messenge => "thanks for visiting our website", from: "ashwinakgandhi@gmail.com")

  end
end
